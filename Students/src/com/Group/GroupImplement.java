package com.Group;


import com.StudentProgress.StProgress;

public class GroupImplement implements Group {

    StProgress stProgress = new StProgress();

    @Override
    public void average() {
        double sum = 0;
        double sred = 0;
        int count = 0;

        for (int i = 0; i<stProgress.ocenka.length; i++) {
            sum += stProgress.ocenka[i];
            count++;

        }
        sred = sum / count;
        System.out.println("Cредний балл учебной группы = " +sred);

    }

    @Override
    public void averagest() {

        for (int i = 0; i<stProgress.ocenka.length; i++){
            System.out.print(" " +stProgress.name[i] + " " + stProgress.ocenka[i]);
        }
        System.out.println();

    }

    @Override
    public void well() {

        for (int i = 0; i<stProgress.ocenka.length; i++){

            if (stProgress.ocenka[i] == 5) {
                System.out.print("Отличник: ");
                System.out.println(stProgress.name[i]);
            }

        }

    }

    @Override
    public void losers() {

        for (int i = 0; i<stProgress.ocenka.length; i++) {

            if (stProgress.ocenka[i] == 2) {
                System.out.print("Двоешник: ");
                System.out.println(stProgress.name[i]);
            }

        }

    }
}
