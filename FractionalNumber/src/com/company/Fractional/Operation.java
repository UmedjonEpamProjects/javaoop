package com.company.Fractional;

public class Operation {

    //Метод умножение!!
    public void multiplication(int a, int b, int c, int d) {
        int top, bottom;
        String one = "(" + a + "/" + b + ") * ";
        String two = "(" + c + "/" + d + ")";

        if (a != 0 && b != 0 && c != 0 && d != 0) {
            top = a * c;
            bottom = b * d;

            int newTop = simplify(top, bottom);
            int newBottom = simplify(bottom, top);

            System.out.println(one + two + " = " + newTop + "/" + newBottom);
        } else
            System.out.println("!!!");

    }

    //Метод деление!!
    public void division(int a, int b, int c, int d) {
        int top, bottom;
        String one = "(" + a + "/" + b + ") / ";
        String two = "(" + c + "/" + d + ")";

        if (a != 0 && b != 0 && c != 0 && d != 0) {
            top = a * d;
            bottom = b * c;

            int newTop = simplify(top, bottom);
            int newBottom = simplify(bottom, top);

            System.out.println(one + two + " = " + newTop + "/" + newBottom);
        } else
            System.out.println("!!!");

    }

    //Метод сложение!!
    public void addition(int a, int b, int c, int d) {
        int top, bottom;
        String one = "(" + a + "/" + b + ") + ";
        String two = "(" + c + "/" + d + ")";

        if (a != 0 && b != 0 && c != 0 && d != 0) {
            if (b == d) {
                top = a + c;
                bottom = b;
            } else {
                top = a * d + c * b;
                bottom = b * d;
            }

            int newTop = simplify(top, bottom);
            int newBottom = simplify(bottom, top);

            System.out.println(one + two + " = " + newTop + "/" + newBottom);
        } else
            System.out.println("!!!");
    }

    //Метод вычитание!!
    public void subtraction(int a, int b, int c, int d) {
        int top, bottom;
        String one = "(" + a + "/" + b + ") - ";
        String two = "(" + c + "/" + d + ")";

        if (a != 0 && b != 0 && c != 0 && d != 0) {
            if (b == d) {
                top = a - c;
                bottom = b;
            } else {
                top = a * d - c * b;
                bottom = b * d;
            }

            int newTop = simplify(top, bottom);
            int newBottom = simplify(bottom, top);

            System.out.println(one + two + " = " + newTop + "/" + newBottom);
        } else {
            System.out.println("!!!");
        }
    }

    //Метод упрощение
    public int simplify(int a, int b) {
        int e = 2;
        int i = 0;
        int max = a > b ? a : b;

        while (i < max) {
            if (a % e == 0 && b % e == 0) {
                a /= e;
                b /= e;
                continue;
            }
            e += 1;
            i += 1;
        }

        return a;
    }
}

