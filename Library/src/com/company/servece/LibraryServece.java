package com.company.servece;

import com.company.model.Library;

import java.util.*;

public class LibraryServece {

    //метод добавление книги
    public void addbook(ArrayList<String> book , String books) {

        book.add(books);

    }

    //метод добавление Автора
    public void addauthor(ArrayList<String> author , String authors) {

        author.add(authors);

    }

    //метод удаления по индексу, индекс равно 0 , в массиве книг удаляем "Сказки" а в автором удаляем "Пушкина"!
    public void delete(int index, ArrayList<String> list) {
        if (index >= 0 && index < list.size()) {
            list.remove(index);
        } else {
            System.out.println("Такого индекса нет");
        }
    }

    // метод поиска по индексу!
    public String search(int index, ArrayList<String> list) {
        for (int i = 0; i < list.size(); i++) {
            if (index >= 0 && index < list.size()) {
                return list.get(index);
            }else {
                System.out.println("Нет такого элемента");
            }
        }
        return null;
    }

    //ВЫВОД
    public void printu(ArrayList<String> print) {
        for (int i = 0; i < print.size(); i++) {
            System.out.println(print.get(i));
        }
    }
}