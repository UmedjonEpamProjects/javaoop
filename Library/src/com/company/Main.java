package com.company;


import com.company.model.Library;
import com.company.servece.LibraryServece;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        LibraryServece libs = new LibraryServece();
        Library library = new Library();

        System.out.println("НАЗВАНИЕ КНИГ");

        libs.addbook(library.book, "Сказки");
        libs.addbook(library.book, "Война и Мир");
        libs.addbook(library.book, "Отцы и Дети");

        //для удаления нужно указать индекс массива книг
        //данный момент индекс = 0 и удаляется первый массив книг
        libs.delete(0,library.book);

        //поиск книг по индексу, ищем второй массив индекса и выводим
        System.out.println(libs.search(1, library.book));

        //Вывод
        libs.printu(library.book);

        System.out.println("------------");

        System.out.println("ИМЕНА АВТОРОВ!");
        libs.addauthor(library.author, "Пушкин");
        libs.addauthor(library.author, "Толстой");
        libs.addauthor(library.author, "Тургенев");

        //для удаления нужно указать индекс массива авторов
        //данный момент индекс = 0 и удаляется первый массив авторов
        libs.delete(0,library.author);

        //поиск авторов по индексу
        System.out.println(libs.search(1, library.author));

        //Вывод
        libs.printu(library.author);
    }
}
